using System.Collections;
using System.Collections.Generic;
using Leogame.IAP;
using UnityEngine;
using UnityEngine.Purchasing;

public class StoreListenerTester : MonoBehaviour, IDetailedStoreListenerLG
{
    [SerializeField] List<ShopPackage> shopPackages;

    public List<ShopPackage> GetShopPackages()
    {
        return shopPackages;
    }

    public void OnGetProductsMetadata(Product[] products)
    {
        foreach(var p in products)
        {
            Debug.Log(p.definition.id + " " + p.metadata.localizedPriceString);
        }
    }

    public void OnPurchaseFailed()
    {

    }

    public void OnPurchaseProductComplete(string productID)
    {

    }

    public void OnRestoreProductComplete(string productID)
    {

    }

    public void OnRestoreResult(string result)
    {

    }

}
