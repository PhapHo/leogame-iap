using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Leogame.IAP
{
    public interface IDetailedStoreListenerLG
    {
        public List<ShopPackage> GetShopPackages();

        public void OnGetProductsMetadata(UnityEngine.Purchasing.Product[] products);

        public void OnPurchaseProductComplete(string productID);

        public void OnRestoreProductComplete(string productID);

        public void OnPurchaseFailed();

        public void OnRestoreResult(string result);
    }
}