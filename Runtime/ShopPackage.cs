using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Leogame.IAP
{
    public enum ShopItemType
    {
        NoAds,
        Coin
    }

    [Serializable]
    public class ShopItem
    {
        public ShopItemType shopItemType;
        public int amount;
    }

    public enum PackageType : int
    {
        Consumable,
        NonConsumable,
        Subscription,
        WatchAds
    }

    [Serializable]
    public class ShopPackage
    {
        public int id;
        public bool active;
        public string name;
        public string productId;
        public PackageType type;
        public List<ShopItem> items;

        public ShopPackage(int id, string name, string productId, PackageType type, List<ShopItem> items)
        {
            this.id = id;
            this.name = name;
            this.productId = productId;
            this.type = type;
            this.items = items;
        }

        public ShopPackage Clone()
        {
            ShopPackage rs = new (id, name, productId, type, items);
            return rs;
        }

        public string ToJson()
        {
            var rs = JsonConvert.SerializeObject(this);
            return rs;
        }

        public bool ContainsRemoveAds()
        {
            ShopItem removeAds = items.Find(x => x.shopItemType.Equals(ShopItemType.NoAds));
            return removeAds != null;
        }

        public bool CoinOnly()
        {
            return items.Count == 1 && items[0].shopItemType == ShopItemType.Coin;
        }

        public bool Is(string productId)
        {
            return this.productId == productId;
        }

        public bool IsWatchAdsPackage()
        {
            return type == PackageType.WatchAds;
        }

        public int CoinAmount()
        {
            ShopItem coinItem = items.Find(x => x.shopItemType == ShopItemType.Coin);

            if (coinItem != null)
            {
                return coinItem.amount;
            }
            return 0;
        }

    }

    [Serializable]
    public class ShopPackageList
    {
        public List<ShopPackage> packages;
    }
}