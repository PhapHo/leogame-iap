using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;

namespace Leogame.IAP
{
    public class IAPManager : MonoBehaviour, IDetailedStoreListener
    {
        private IStoreController controller;    
        private IExtensionProvider extensions;
        private ConfigurationBuilder builder;
        private bool isPurchase = false;
        [Space]
        [SerializeField] IDetailedStoreListenerLG detailedStoreListener;

        private void Awake()
        {
            detailedStoreListener = GetComponent<IDetailedStoreListenerLG>();
            Init();
            ListenerManager.Instance.Register(ListenType.GetAnalyticsInstanceId, OnGetAnalyticsInstanceId);
        }

        public void Init()
        {
            List<ShopPackage> packages = detailedStoreListener.GetShopPackages();

            builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            foreach (ShopPackage package in packages)
            {
                if (!package.IsWatchAdsPackage())
                    builder.AddProduct(package.productId, package.type.GetProductType());
            }

            UnityPurchasing.Initialize(this, builder);
        }

        private void OnGetAnalyticsInstanceId(object data)
        {
            var ggPlayConfig = builder.Configure<IGooglePlayConfiguration>();
            string id = (string)data;
            if (id != null)
            {
                ggPlayConfig.SetObfuscatedAccountId(id);
            }
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("In-App Purchasing successfully initialized");
            this.controller = controller;
            this.extensions = extensions;
            GetProductsMetadata();
        }

        public void GetProductsMetadata()
        {
            detailedStoreListener.OnGetProductsMetadata(controller.products.all);
        }

        public string GetRemoveAdsPrice(string removeAdsProductID)
        {
            foreach (var product in controller.products.all)
            {
                if (product.definition.id == removeAdsProductID)
                    return product.metadata.localizedPriceString;
            }
            return null;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError($"In-App Purchasing initialize failed: {error}");
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            var product = args.purchasedProduct;

            if (isPurchase)
            {
                detailedStoreListener.OnPurchaseProductComplete(product.definition.id);
                isPurchase = false;
            }
            else
            {
                detailedStoreListener.OnRestoreProductComplete(product.definition.id);
            }

            Debug.Log($"<color=yellow>Purchase Complete - Product: {product.definition.id} </color>");

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.Log($"Purchase failed - Product: '{product.definition.id}', PurchaseFailureReason: {failureReason}");
        }

        public void OnPurchaseClicked(string productId)
        {
            isPurchase = true;

            controller.InitiatePurchase(productId);
        }

        public void RestorePurchases()
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                Debug.Log("RestorePurchases started ...");

                var apple = extensions.GetExtension<IAppleExtensions>();

                apple.RestoreTransactions((result, message) =>
                {
                    if (result)
                    {

                    }
                    else
                    {
                        detailedStoreListener.OnRestoreResult("Restore failed");
                    }
                });
#if UNITY_IOS
            RefreshAppReceipt(controller, extensions);
#endif
            }
        }

        public void OnInitializeFailed(InitializationFailureReason error, string message)
        {
            Debug.LogError("IAP OnInitializeFialed " + error + " " + message);
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureDescription failureDescription)
        {
            Debug.Log($"Purchase failed - Product: '{product.definition.id}', PurchaseFailureReason: {failureDescription.reason}");
            detailedStoreListener.OnPurchaseFailed();
        }


    public void RefreshAppReceipt(IStoreController controller, IExtensionProvider extensions)
    {
        extensions.GetExtension<IAppleExtensions>().RefreshAppReceipt(result => {

            var receiptData = Convert.FromBase64String(result);
            AppleReceipt receipt = new AppleValidator(AppleTangle.Data()).Validate(receiptData);

            Debug.Log(receipt.bundleID + receipt.receiptCreationDate);

            foreach (AppleInAppPurchaseReceipt productReceipt in receipt.inAppPurchaseReceipts)
            {
                Debug.Log(productReceipt.transactionID + productReceipt.productID);
            }

            string notice = receipt.inAppPurchaseReceipts.Length > 0 ? "Restore purchases success!" : "You don't have any purchases!";

            detailedStoreListener.OnRestoreResult(notice);
        },
        (err) => {
            Debug.LogError("RefreshAppReceipt Error" + err);
        });
    }


    }

    public static class IAPExtension
    {
        public static ProductType GetProductType(this PackageType type)
        {
            switch (type)
            {
                case PackageType.Consumable:
                    return ProductType.Consumable;
                case PackageType.NonConsumable:
                    return ProductType.NonConsumable;
                case PackageType.Subscription:
                    return ProductType.Subscription;
                default:
                    throw new Exception();
            }
        }
    }
}